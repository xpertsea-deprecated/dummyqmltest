SOURCES += \
    main.cpp

QT += core qml quick websockets network quickcontrols2 svg xml testlib

QT -= gui

TARGET = tests-qmltests

CONFIG += warn_on qmltestcase console

DISTFILES += \
    tst_dummyTest.qml
