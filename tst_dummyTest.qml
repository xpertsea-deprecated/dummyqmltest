import QtQuick 2.7
import QtTest 1.1
import QtQuick.Controls 2.0

Rectangle {
    width: 640; height: 480

    MouseArea {
        id: area
        anchors.fill: parent

        property bool touched: false

        onPressed: touched = true
    }

    TestCase {
        name: "ItemTests"
        when: windowShown
        id: test1

        function test_touch() {
            var touch = mouseClick(area);
            verify(area.touched);
        }
    }
}
