container_name=qt
image=xpertsea/qt:5.10.1

docker kill $container_name &> /dev/null
docker rm $container_name &> /dev/null

bash_command='for files in $(find build/linux-clang -type f -name tests-* -perm /u=x,g=x,o=x);
do
	./$files || exit 1;
done'

command=$bash_command' '$@
xserver="-v /tmp/.X11-unix:/tmp/.X11-unix"
display="-e DISPLAY=$DISPLAY"
volumes="-v /"${PWD}":/xpertsea"
user="-e LOCAL_USER_ID=`id -u $USER`"

mkdir -p $buildfolder
docker run --rm $xserver $display -it --name $container_name $volumes $user $image bash -c "$bash_command" $@
