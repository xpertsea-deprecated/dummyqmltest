container_name=qt
image=xpertsea/qt:5.10.1

docker kill $container_name &> /dev/null
docker rm $container_name &> /dev/null

buildfolder=build/linux-clang
bash_command="cd $buildfolder && /opt/qt/5.10.0/gcc_64/bin/qmake /xpertsea/simpleQMLTest.pro -r -spec linux-clang CONFIG+=debug CONFIG+=qml_debug && make -j4"
command=$bash_command' '$@
volumes="-v /"${PWD}":/xpertsea"
user="-e LOCAL_USER_ID=`id -u $USER`"

mkdir -p $buildfolder
docker run --rm --name $container_name $volumes $user $image bash -c "$command"
