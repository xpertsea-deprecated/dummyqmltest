/*
 * Copyright (c) 2018 XpertSea Solutions Inc.
 *
 * All rights reserved.
 *
 * @author vasile
 * @date 2018-02-02
 */

#include <QtQuickTest/quicktest.h>

int main(int argc, char **argv) {
  QTEST_ADD_GPU_BLACKLIST_SUPPORT
  QTEST_SET_MAIN_SOURCE_PATH
  return quick_test_main(argc, argv, "QMLViews", QUICK_TEST_SOURCE_DIR);
}
